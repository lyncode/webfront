package routes

// all routes registering
// user router
// doc router
// probe router

import "github.com/gorilla/mux"

func InitRoutes() *mux.Router {
	router := mux.NewRouter().StrictSlash(false)
	router = SetUserRoutes(router)
	router = SetDocRoutes(router)
	router = SetProbeUser(router)
	return router
}
