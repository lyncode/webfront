package routes

import (
	"github.com/gorilla/mux"
	"wikiWeb/handlers"
)

// register for new user

// login for existed user

func SetUserRoutes(router *mux.Router) *mux.Router {
	router.HandleFunc("/user/register", handlers.Register).Methods("POST")
	router.HandleFunc("/user/login", handlers.Login).Methods("POST")
	return router
}
