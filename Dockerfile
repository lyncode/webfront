FROM golang:1.12.7-alpine3.9 AS builder
MAINTAINER LYN "lynmeig@live.com"
WORKDIR /go/src/wikiWeb
RUN apk update && apk upgrade && apk add git
ADD . .
RUN go get github.com/golang/dep/cmd/dep
RUN dep init && dep ensure
# do not dep again if the project already have the vendor
# using .gitignore is a solution for vendors
RUN go build main.go


# multiple stage build to reduce the image size
FROM alpine:latest as prod
RUN apk --no-cache add ca-certificates
WORKDIR /wikiWeb
COPY  --from=builder /go/src/wikiWeb /wikiWeb/
EXPOSE 8080
ENTRYPOINT ["./main"]