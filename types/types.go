package types

// all data type list in here

type User struct {
	// user definition
	Id           string `json:"uuid"`
	Email        string `json:"email"`
	PassWord     string `json:"password,omitempty"`
	HashPassWord []byte `json:"hash_password,omitempty"`
}

type Login struct {
	// login data type when user request login
	// user provide email and password
	Email    string `json:"email"`
	Password string `json:"password"`
}

type AuthedUser struct {
	// authenticate successful user will get a token for the rest api access
	User  User   `json:"user"`
	Token string `json:"token"`
}

type WikiDoc struct {
	// the wikipedia documents content return to user
	Link      string `json:"link"`
	Keyword   string `json:"keyword"`
	Paragraph string `json:"paragraph"`
}

type KeyWordResponse struct {
	// return the keyword request from user
	Notation string      `json:"notation"`
	KeyWords interface{} `json:"keyword"`
}

type AppError struct {
	Error      string `json:"error"`
	Message    string `json:"message"`
	HttpStatus int    `json:"status"`
}

type ErrorResource struct {
	Data AppError `json:"data"`
}
