package main

import (
	"github.com/codegangsta/negroni"
	"log"
	"net/http"
	"wikiWeb/config"
	"wikiWeb/dbOps"
	"wikiWeb/routes"
	"wikiWeb/utils"
)

func main() {
	utils.StartUp()
	// do web server preparation works
	go dbOps.CheckConnPool()
	// infinite loop to maintain the database connection pool

	router := routes.InitRoutes()
	n := negroni.Classic()
	n.UseHandler(router)
	server := &http.Server{
		Addr:    config.WebConfig.Server,
		Handler: n,
	}
	log.Println("Listening....")
	_ = server.ListenAndServe()
}
