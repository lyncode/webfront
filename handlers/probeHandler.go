package handlers

import (
	"net/http"
)

// simple liveness & readiness probe for kubernetes monitoring
func Healthz(w http.ResponseWriter, r *http.Request) {
	w.WriteHeader(http.StatusOK)
	_, _ = w.Write([]byte("web is alive."))
}
