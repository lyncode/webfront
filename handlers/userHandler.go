package handlers

import (
	"encoding/json"
	"golang.org/x/crypto/bcrypt"
	"net/http"
	"wikiWeb/config"
	"wikiWeb/dbOps"
	"wikiWeb/types"
	"wikiWeb/utils"
)

func Register(w http.ResponseWriter, r *http.Request) {
	var user types.User
	err := json.NewDecoder(r.Body).Decode(&user)
	if err != nil {
		utils.DisplayAppError(w, err, "Parse User Data Fail",
			http.StatusInternalServerError)
		return
	}
	client := <-dbOps.ConnectionPool
	userColl := client.Database(config.WebConfig.DataBase).Collection("users")
	_, err = dbOps.CheckUser(userColl, user)
	// existed user do not allow register again

	if err != nil {
		utils.DisplayAppError(w, err, "User Already exist,please use your user name",
			http.StatusInternalServerError)
		dbOps.ConnectionPool <- client
		return
	}

	if err = dbOps.CreateUser(userColl, &user); err != nil {
		utils.DisplayAppError(w, err, "Create User Fail",
			http.StatusInternalServerError)
		dbOps.ConnectionPool <- client
		return
	}
	user.HashPassWord = nil
	if j, err := json.Marshal(user); err != nil {
		utils.DisplayAppError(w, err, "unexpected error",
			http.StatusInternalServerError)
		dbOps.ConnectionPool <- client
		return
	} else {
		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(http.StatusCreated)
		_, _ = w.Write(j)
		dbOps.ConnectionPool <- client
	}
}

func Login(w http.ResponseWriter, r *http.Request) {
	var login types.Login
	var token string
	var user types.User
	err := json.NewDecoder(r.Body).Decode(&login)
	if err != nil {
		utils.DisplayAppError(w, err, "invalid login data format",
			http.StatusInternalServerError)
		return
	}
	client := <-dbOps.ConnectionPool
	userColl := client.Database(config.WebConfig.DataBase).Collection("users")
	findUser, err := dbOps.FindUser(userColl, login)
	if err != nil {
		utils.DisplayAppError(w, err, "User not found", http.StatusInternalServerError)
		dbOps.ConnectionPool <- client
		return
	}
	err = bcrypt.CompareHashAndPassword(findUser.HashPassWord, []byte(login.Password))
	// compare the hashed password equal or not

	if err != nil {
		utils.DisplayAppError(w, err, "Invalid credential", http.StatusUnauthorized)
		dbOps.ConnectionPool <- client
		findUser = types.User{}
		return
	}
	token, err = utils.GenerateJWT(login.Email)
	if err != nil {
		utils.DisplayAppError(w, err, "Error while generating access token",
			http.StatusInternalServerError)
		return
	}
	w.Header().Set("Content-Type", "application/json")
	user.HashPassWord = nil
	user.Id = findUser.Id
	user.Email = findUser.Email
	authUser := types.AuthedUser{
		User:  user,
		Token: token,
	}
	j, err := json.Marshal(authUser)
	if err != nil {
		utils.DisplayAppError(w, err, "An unexpected error has occur",
			http.StatusInternalServerError)
		return
	}
	w.WriteHeader(http.StatusOK)
	_, _ = w.Write(j)

}
