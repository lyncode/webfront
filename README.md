# Web REST API Application Explain

## this is a sub-project of one [micro-service project](https://gitlab.com/lyncode/devops)

## Application Components
This is a web rest api application based on stack as follows
- [golang](https://golang.org/)
  - sure the project is written in golang
- [negroni](https://github.com/urfave/negroni)
  - negroni provide a simple approach to add middleware to web routes
- [gorilla](https://www.gorillatoolkit.org/)
  - gorilla web toolkit provide a mux package which can easily set a bunch a routes
- [mongoDB](https://www.mongodb.com/)
  - mongoDB provide data persistent storage
  - the advantage of mongoDB is the schema free feature and easy to sharding
- [jwt](https://github.com/dgrijalva/jwt-go)
  - using RSA algorithm to generate (JWT)json web token to auth user
- [Docker](https://www.docker.com/)
  - build and distribute docker image in case of cloud deployment
- [gitlab-ci/cd](https://docs.gitlab.com/ee/ci/)
  - Core of the DevOps, which is a script that automatically executed by gitlab runner every time you push code to gitlab

## Architecture
![arch](images/architecture.png)
 REST API features
 - jwt encryption
 - https encryption
   - [RSA](https://en.wikipedia.org/wiki/RSA_(cryptosystem))
   - [let's encrypt](https://letsencrypt.org/)

## Api Explain
### Register
- register
- explain
  - register api
    - ```POST```the url```host.domain.level/user/register``` with **request body** shown in the figure,and the server will return your email address and your uuid
    - ![register](images/register.png)
  
  - register again
    - If you already registered, server will notify you this
    - ![registerAgain](images/registerAgain.png)
### Login
- login
- explain
  - ```POST``` the url ```host.domain.level/user/login``` with the previous email address and password in the request body
  - and you will get a jwt token, this token responsible for the every rest single http request, or you will get unauthorized error

  - ![login](images/login.png)
### Request Keyword
- request a list of keywords for further exploration 
- ```GET```the url ```host.domain.level/keyword``` with the JWT return from the login,
you can get a random part of keywords, and you can select one of them to reach the content of this wikipedia document
  - ![keyword](images/keyword.png)
### Request Document
- request a document from server
- ```GET``` the url ```host.domain.level/doc/{your keyword}``` ,also the jwt attached in the request header
  - ![doc](images/doc.png)