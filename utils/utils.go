package utils

import (
	"encoding/json"
	"log"
	"net/http"
	"wikiWeb/config"
	"wikiWeb/dbOps"
	"wikiWeb/types"
)

func DisplayAppError(w http.ResponseWriter, handlerError error, message string, code int) {
	// helper function for conveniently return error info to user
	errObj := types.AppError{
		Error:      handlerError.Error(),
		Message:    message,
		HttpStatus: code,
	}
	log.Printf("[AppError]:%s\n", handlerError)
	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	w.WriteHeader(code)
	// http status code
	if j, err := json.Marshal(types.ErrorResource{Data: errObj}); err == nil {
		_, _ = w.Write(j)
	}
}

func StartUp() {
	// server starting preparation
	config.LoadConfig()
	initKeys()
	dbOps.InitConnPool()
}
